package com.quangsang.springbootjenkinscicd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootJenkinsCicdApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootJenkinsCicdApplication.class, args);
    }

}
