package com.quangsang.springbootjenkinscicd;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.TimeUnit;

@Slf4j
@RestController
@RequestMapping("/api/tests")
public class TestController {

    @GetMapping("/test1")
    public String test1(){
        log.info("log lever info test 1");
        return "test1";
    }

    @GetMapping("/test2")
    public String test2(){
        log.info("log lever info test 2");
        return "test2";
    }

    @GetMapping("/test3")
    public String test3(){
        log.info("log lever info test 3");
        return "test3";
    }
}
